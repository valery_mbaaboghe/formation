'use strict';
describe('status filters', function() {
  beforeEach(module('app'));
  describe('isLate', function() {

    it('should accept a task if it is late', inject(function(isLateFilter) {
      expect(isLateFilter({
        plannedFor: new Date().getTime() - 1
      })).toBeTruthy();
    }));
    it('should refuse a task if it is not late', inject(function(isLateFilter) {
      expect(isLateFilter({
        plannedFor: new Date().getTime() + 1
      })).toBeFalsy();
    }));
  });
  describe('isShort', function() {

    it('should accept a task if it is less than 2 minutes long', inject(function(isShortFilter) {
      expect(isShortFilter({
        duration: 2
      })).toBeTruthy();
      expect(isShortFilter({
        duration: 1.9
      })).toBeTruthy();
    }));
    it('should refuse a task if it is more than 2 minutes long', inject(function(isShortFilter) {
      expect(isShortFilter({
        duration: 2.01
      })).toBeFalsy();
    }));
  });
  <!-- TODO "remaining" filter tests -->
});
